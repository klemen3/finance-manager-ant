package com.klemen.finance_manager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import net.miginfocom.swing.MigLayout;

// TODO
public class Charts {

	private static JFrame chartsFrame;

	public void drawChartWindow() {
		chartsFrame = new JFrame("Chart");

		PieDataset dataset = createDataset();

		JFreeChart chart = ChartFactory.createPieChart("Percentages", dataset, true, true, false);

		PieSectionLabelGenerator labelGenerator = new StandardPieSectionLabelGenerator("Marks {0} : ({2})",
				new DecimalFormat("0"), new DecimalFormat("0%"));
		((PiePlot) chart.getPlot()).setLabelGenerator(labelGenerator);

		// Create Panel
		ChartPanel chartsPanel = new ChartPanel(chart);

		chartsFrame.add(chartsPanel);

		// Frame settings
		chartsFrame.setLocationRelativeTo(null);
		chartsFrame.setVisible(true);
		chartsFrame.setSize(800, 400);
		chartsFrame.pack();
		chartsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private PieDataset createDataset() {

		HashMap<String, Double> hPercentages = DAO.getPercentages(); 
		
		DefaultPieDataset dataset = new DefaultPieDataset();
		
		for (Map.Entry<String, Double> entry : hPercentages.entrySet()) {
//		    dataset.setValue(hPercentages.get(entry.getKey()), hPercentages.get(entry.getValue()*100));
			String name = entry.getKey();
			double temp = hPercentages.get(entry.getKey());
			temp = temp * 100;
			 dataset.setValue(name, temp);
		}
		
		return dataset;
	}

}
