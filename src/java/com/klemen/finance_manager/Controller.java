package com.klemen.finance_manager;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

public class Controller {
	
	public static void saveButtonAction() throws SQLException {

		String sType = Gui.inputComboTransactionType.getSelectedItem().toString();
		String sValueText = Gui.inputTransactionValue.getText();
		String sCategory;
		
		if (Gui.inputComboCategory.isEnabled()) {
			sCategory = Gui.inputComboCategory.getSelectedItem().toString();
		} else {
			sCategory = null;
		}
		

		float fValue;

		try {
			fValue = Float.parseFloat(sValueText);
			String sDescription = Gui.inputDescription.getText();

			JFormattedTextField editor = Gui.inputDatePicker.getEditor();
			Date dateIDP = (Date) editor.getValue();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String sDate = format.format(dateIDP);
			AppLogger.log(Level.FINE, "Value: " + fValue);
			
			System.out.println("Date: " + sDate);
			System.out.println("Category: " + sCategory);
			
			if (fValue < 0) {
				AppLogger.log(Level.INFO, "Input 'Value' must be a POSITIVE real number.");
				JOptionPane.showMessageDialog(null, "Input 'Value' must be a POSITIVE real number.");
			} else {
				if (sType.equals("Income")) {
					AppLogger.log(Level.FINE, "v1: " + fValue);
					DAO.insert(fValue, sCategory, sDescription, sDate);
				} else {
					AppLogger.log(Level.FINE, "v2: " + fValue);
					DAO.insert(-fValue, sCategory, sDescription, sDate);
				}
			}
			// Set text in Gui for TotalBalance, MonthlyBalance, MoneyLeft
			Gui.textTotalBalance.setText(String.valueOf(DAO.selectTotalBalance()));
			Gui.textMonthlyBalance.setText(String.valueOf(DAO.selectMonthlyBalance()));
			Gui.textMoneyLeft.setText(String.valueOf(DAO.monthlyBudgetLeft()));
			
		} catch (Exception e) {
			AppLogger.log(Level.INFO, e.toString());
			JOptionPane.showMessageDialog(null, "Input 'Value' must be a real number");
		}
	}

	public static void refreshButtonAction() throws SQLException {
		Gui.textTotalBalance.setText(String.valueOf(DAO.selectTotalBalance()));
		Gui.textMonthlyBalance.setText(String.valueOf(DAO.selectMonthlyBalance()));
		Gui.textMoneyLeft.setText(String.valueOf(DAO.monthlyBudgetLeft()));
		DAO.selectDebug();
	}
	
	/*
	 *  klik na gumb Save v Settings oknu
	 *  poklice funkcijo iz ConnectionManager, ki iz podatkov, ki so v bazi,
	 *  izracuna, koliko denarja je se ostalo za ta mesec
	 */
	public static void saveSettingsButtonAction() {
		try {
			double budget = Double.parseDouble(SettingsWindow.inputMonthlyGoal.getText());
			if (budget <= 0) {
				AppLogger.log(Level.INFO, "Input 'Monthly budget' must be a POSITIVE real number.");
				JOptionPane.showMessageDialog(null, "Input 'Monthly budget' must be a POSITIVE real number.");
			} else {
				// Save budget to db
				DAO.insertSettings(budget);
				// Get budget from db and set text in Gui
				String moneyLeft = DAO.monthlyBudgetLeft();
				AppLogger.log(Level.FINE, "Monthly budget left: " + moneyLeft);
				Gui.textMoneyLeft.setText(moneyLeft);
			}
		} catch (Exception e2) {
			AppLogger.log(Level.INFO, "Input 'Monthly budget' must be a real number");
			JOptionPane.showMessageDialog(null, "Input 'Monthly budget' must be a real number");
		}
	}

	// Set settings input fields to already defined values if they exist,
	// otherwise to some initial values.
	public static void openSettingsButtonAction() {
		try {
			SettingsWindow.inputMonthlyGoal.setText(String.valueOf(DAO.getMonthlyBudgetSetting()));
		} catch (SQLException e) {
			AppLogger.log(Level.SEVERE, e.toString());
			JOptionPane.showMessageDialog(null, "SQL exception occured: " + e);
		}
		
	}
	
}
