package com.klemen.finance_manager;

import java.math.RoundingMode;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;

import javax.swing.JOptionPane;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class DAO {
	
	private static Connection conn          = null;
	private static Statement stmt           = null;
	private static CallableStatement cstmt  = null;
	private static PreparedStatement pstmt  = null;
	private static ResultSet rs  		    = null;

	// Queries
	private static String qCreateTransactionTable;
	private static String qCreateSettingsTable;
	private static String qInsertTransaction; // NOT USED
	private static String qSelectMonthlyBalance;
	private static String qSelectMonthlyBudget;
	private static String qGetBudgetSetting;
	private static String qCheckIfSettingsExist;
	private static String qUpdateSettings;
	private static String qInsertSettings;
	private static String qSelectDebug;
	private	static String qGetCategories;
	
	
	private static String sDbDriver;
	private static String sDbURL;
	private static String sDbUser;
	private static String sDbPassword;
	
	// Rounding format
	private static DecimalFormat roundingFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US));
	
	
	// Inject properties and set rounding mode
	static {		
		PropertiesConfiguration config;
		try {
			config = new PropertiesConfiguration("application.properties");
			
			qCreateTransactionTable = config.getString("createTransactionTableQuery");
			qCreateSettingsTable    = config.getString("createSettingsTableQuery");
			qInsertTransaction      = config.getString("insertTransactionQuery");
			qSelectMonthlyBalance   = config.getString("selectMonthlyBalanceQuery");
			qSelectMonthlyBudget    = config.getString("selectMonthlyBudgetQuery");
			qGetBudgetSetting       = config.getString("getBudgetQuery");
			qCheckIfSettingsExist   = config.getString("checkIfSettingsExistQuery");
			qUpdateSettings         = config.getString("updateSettingsQuery");
			qInsertSettings         = config.getString("insertSettingsQuery");
			qSelectDebug            = config.getString("selectDebugQuery");
			qGetCategories			= config.getString("getCategoriesQuery");
			
			sDbDriver				= config.getString("db.driver");
			sDbURL					= config.getString("db.url");
			sDbUser 				= config.getString("db.user");
			sDbPassword				= config.getString("db.password");
			
			AppLogger.log(Level.INFO, "Config loaded.");
			
			roundingFormat.setRoundingMode(RoundingMode.HALF_EVEN);
			
			
		} catch (ConfigurationException ce) {
			AppLogger.log(Level.SEVERE, ce.toString());
			JOptionPane.showMessageDialog(null, "Exception occured: " + ce.toString());
		}
	}
	
	
	// Connect to database
	public static void connect() throws SQLException {
		try {
			Class.forName(sDbDriver);
			String url = sDbURL + ", " + sDbUser + ", " + sDbPassword;
			System.out.println("URL: " + url);
			conn = DriverManager.getConnection(sDbURL, sDbUser, sDbPassword);
			stmt = conn.createStatement();
			AppLogger.log(Level.INFO, "connected to database...");
		} catch (ClassNotFoundException e) {
			AppLogger.log(Level.SEVERE, e.toString());
			JOptionPane.showMessageDialog(null, e);;
		}
	}

	// NOT USED
	public static void createTable() throws SQLException {
		stmt = conn.createStatement();
		stmt.executeUpdate(qCreateTransactionTable);
		stmt.executeUpdate(qCreateSettingsTable);
		
		AppLogger.log(Level.INFO, "Table created.");
	}

	// NOT USED
	public static void insert(double dAmount, String sDescription, String sDate) throws SQLException {
		pstmt = conn.prepareStatement(qInsertTransaction);
		pstmt.setDouble(1, dAmount);
		pstmt.setString(2, sDescription);
		pstmt.setString(3, sDate);
		pstmt.executeUpdate();
		
		AppLogger.log(Level.INFO, "Row inserted");
	}
	
	// Insert transaction
	public static void insert(float fAmount, String sCategory, String sDescription, String sDate) {
		try {
			cstmt = conn.prepareCall("{call fm.F_INSERT_TRANSACTION(?,?,?,?) }");
			cstmt.setFloat(1, fAmount);
			cstmt.setString(2, sCategory);
			cstmt.setString(3, sDescription);
			cstmt.setString(4, sDate);
			cstmt.execute();
		} catch (SQLException sqe) {
			AppLogger.log(Level.SEVERE, sqe.toString());
		}
		
	}
	
	// Get expense by categories
	public static HashMap<String, Double> getPercentages() {
		
		ArrayList<Double> aResultPercentages = new ArrayList<>();
		ArrayList<String> aResultCategories = new ArrayList<>();
		
		HashMap<String, Double> hResult = new HashMap<>();
		
		try {
			cstmt = conn.prepareCall("{call fm.f_get_expense(?, ?) }");
			cstmt.registerOutParameter(1, Types.ARRAY);
			cstmt.registerOutParameter(2, Types.ARRAY);
			cstmt.executeUpdate();
			Array aPercentages = cstmt.getArray(1);
			Array aCategories = cstmt.getArray(2);
			
			Double[] adPercentages = (Double[])aPercentages.getArray();
			String[] adCategories = (String[])aCategories.getArray();
			
			for (int i = 0; i < adPercentages.length; i++) {
				aResultCategories.add(adCategories[i]);
				aResultPercentages.add(adPercentages[i]);
			}			
			
			for (int i = 0; i < adCategories.length; i++) {
				hResult.put(aResultCategories.get(i), aResultPercentages.get(i));
			}
			
		} catch (SQLException sqe) {
			AppLogger.log(Level.SEVERE, sqe.toString());
		}
		
		return hResult;
		
	}

	// Calculate total balance from db and return string of rounded result
	public static String selectTotalBalance() throws SQLException {	
		
		cstmt = conn.prepareCall("{? = call fm.f_GET_TOTAL_BALANCE() }");
		cstmt.registerOutParameter(1, Types.DOUBLE);
		cstmt.execute();
		double dTotalBalance = cstmt.getDouble(1);
		
		String sTotalBalance = roundingFormat.format(dTotalBalance);
		AppLogger.log(Level.FINE, ("Total balance: " + sTotalBalance));
		return sTotalBalance;

	}

	// Calculate monthly balance from db and return string of rounded result
	public static String selectMonthlyBalance() throws SQLException {	
		rs = stmt.executeQuery(qSelectMonthlyBalance);
		double monthlyBalance = 0;
		while (rs.next()) {
			monthlyBalance = rs.getDouble(1);
		}
		String monthlyBalanceStr = roundingFormat.format(monthlyBalance);
		AppLogger.log(Level.FINE, ("Monthly balance: " + monthlyBalanceStr));
		return monthlyBalanceStr;
	}
	
	// Calculate monthly budget left from db and return string of rounded result
	public static String monthlyBudgetLeft() throws SQLException {	
		rs = stmt.executeQuery(qSelectMonthlyBudget);
		double monthlyExpense = 0;
		while (rs.next()) {
			monthlyExpense = rs.getDouble(1);
		}
		// monthlyExpense je negativen, zato pristejemo
		double dBudget = Double.parseDouble(getMonthlyBudgetSetting());
		double dMoneyLeft = dBudget + monthlyExpense;
		
		String sMoneyLeft = roundingFormat.format(dMoneyLeft);
		AppLogger.log(Level.FINE, ("Spending money left for this month: " + sMoneyLeft));
		return sMoneyLeft;
	}
	
	// Get monthly budget setting from db and return string of rounded result
	public static String getMonthlyBudgetSetting() throws SQLException {
		double dBudgetSetting = 0;
		rs = stmt.executeQuery(qGetBudgetSetting);
		while (rs.next()) {
			dBudgetSetting = rs.getDouble(1);
		}
		String sBudgetSetting = roundingFormat.format(dBudgetSetting);
		return sBudgetSetting;
	}
	
	// Save budget to database
	public static void insertSettings(double dBudget) {
		try {
			rs = stmt.executeQuery(qCheckIfSettingsExist);
			if (rs.next()) {
				pstmt = conn.prepareStatement(qUpdateSettings);
				pstmt.setDouble(1, dBudget);
				pstmt.executeUpdate();
			} else {
				pstmt = conn.prepareStatement(qInsertSettings);
				pstmt.setDouble(1, dBudget);
				pstmt.executeUpdate();
			}
			
		} catch (SQLException e) {
			AppLogger.log(Level.SEVERE, e.toString());
			JOptionPane.showMessageDialog(null, "SQL exception occured: " + e);
		}
	}
	
	// Get available categories
	public static ArrayList<String> getCategories() {
		ArrayList<String> lCategories = new ArrayList<>();
		try {
			rs = stmt.executeQuery(qGetCategories);
			while (rs.next()) {
				lCategories.add(rs.getString(1));
			}
		} catch (SQLException se) {
			AppLogger.log(Level.SEVERE, "Exception occured while loading categories: " + se);
		}
		return lCategories;
		
		
	}

	// Select * - for debugging
	public static void selectDebug() throws SQLException {
		rs = stmt.executeQuery(qSelectDebug);
		while (rs.next()) {
			AppLogger.log(Level.FINE, rs.getInt(1)
							 + ", " + rs.getString(2)
							 + ", " + rs.getInt(3)
							 + ", " + rs.getString(4));
		}
	}

	// Close database connection
	public static void closeDatabaseConnection() throws SQLException {
		stmt.close();
		if (pstmt != null) {
			pstmt.close();
		}
		conn.close();
		AppLogger.log(Level.INFO, "database connection closed.");
	}

}
