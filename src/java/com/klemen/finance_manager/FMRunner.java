package com.klemen.finance_manager;

import java.sql.SQLException;
import java.util.logging.Level;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class FMRunner {

	public static void main(String[] args) throws SQLException {
		
		// Default format
		String sLoggingFormat = "%1$tF %1$tT %4$s %5$s%6$s%n";
		
		System.out.println("DBG: test1");
		
		// Load custom logging format
		try {
			PropertiesConfiguration config = new PropertiesConfiguration("application.properties");
			sLoggingFormat = config.getString("log.format");
		} catch (ConfigurationException ce) {
			AppLogger.log(Level.SEVERE, ce.toString());
		}
		
		System.setProperty("java.util.logging.SimpleFormatter.format", sLoggingFormat);
		
		AppLogger.log(Level.INFO, "Starting...");
		
		Gui gui = new Gui();
		gui.drawGui();
		Gui.textTotalBalance.setText(String.valueOf(DAO.selectTotalBalance()));
		Gui.textMonthlyBalance.setText(String.valueOf(DAO.selectMonthlyBalance()));
		Gui.textMoneyLeft.setText(String.valueOf(DAO.monthlyBudgetLeft()));
		
	}
	
}

