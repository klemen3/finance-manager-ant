CREATE SCHEMA IF NOT EXISTS fm;

DROP TABLE fm.tb_finance_transaction CASCADE;
DROP TABLE fm.tb_settings CASCADE;
DROP TABLE fm.tb_categories CASCADE;
DROP TABLE fm.tb_dbg CASCADE;

CREATE TABLE fm.tb_settings ( 
	id SERIAL PRIMARY KEY, 
	budget REAL NOT NULL 
);

CREATE TABLE fm.tb_categories (
	id SERIAL PRIMARY KEY,
	category TEXT
);

CREATE TABLE fm.tb_finance_transaction (
	id SERIAL PRIMARY KEY, 
	amount REAL NOT NULL,
	category_id INTEGER references fm.tb_categories(id),
	description TEXT, 
	transaction_date DATE 
);



CREATE TABLE fm.tb_dbg (
	dbg_id		SERIAL PRIMARY KEY,
	dbg_message TEXT NOT NULL,
	dbg_date	DATE
);



INSERT INTO fm.tb_categories(category) VALUES('Food');
INSERT INTO fm.tb_categories(category) VALUES('Drink');
INSERT INTO fm.tb_categories(category) VALUES('Tech');

-- income
INSERT INTO fm.tb_finance_transaction (amount,description,transaction_date) VALUES (1300,'placa','2018-04-11');
INSERT INTO fm.tb_finance_transaction (amount,description,transaction_date) VALUES (1340,'placa','2018-05-11');
INSERT INTO fm.tb_finance_transaction (amount,description,transaction_date) VALUES (1040.5,'placa','2018-03-11');


-- expense
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-82.89,3,'Nunc sed orci lobortis augue scelerisque','2018-08-20');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-45.61,3,'Donec est. Nunc ullamcorper, velit in','2019-03-03');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.39,3,'dictum eu, placerat eget, venenatis a, magna. Lorem','2019-01-06');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-83.86,1,'orci. Phasellus dapibus quam','2018-11-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-73.12,2,'amet','2018-11-03');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-98.06,1,'Proin non massa non ante bibendum','2018-09-04');


INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-70.88,1,'habitant morbi tristique senectus et netus','2018-05-06');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-25.10,1,'vestibulum, neque sed dictum eleifend, nunc risus varius','2019-04-08');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-88.32,1,'eleifend.','2018-12-25');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-67.75,1,'montes, nascetur ridiculus mus. Aenean eget magna.','2018-08-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-58.45,1,'ut aliquam iaculis, lacus pede sagittis augue, eu','2019-02-07');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-96.03,1,'vitae odio','2018-05-22');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-30.06,1,'nec metus facilisis lorem tristique aliquet. Phasellus','2018-05-21');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-37.15,1,'Phasellus dolor elit, pellentesque','2018-10-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-64.05,1,'non, dapibus rutrum, justo. Praesent','2018-08-17');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-27.38,1,'Sed nunc est, mollis','2018-04-08');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-40.65,1,'tellus.','2019-05-06');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-91.17,1,'vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem','2018-04-03');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-45.06,1,'cubilia','2019-02-14');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-21.66,1,'tellus faucibus leo, in lobortis tellus','2018-05-31');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.91,1,'arcu ac orci. Ut semper','2018-09-02');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-95.81,1,'Nunc mauris. Morbi non','2018-04-25');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-80.83,1,'non, bibendum sed, est. Nunc laoreet lectus','2019-02-05');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-33.02,1,'ac','2018-12-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-43.04,1,'vestibulum lorem, sit amet','2019-01-08');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.00,1,'purus, in molestie tortor nibh','2018-05-29');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-96.18,1,'facilisis vitae, orci. Phasellus dapibus quam quis diam.','2018-09-18');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.74,1,'orci. Donec nibh. Quisque nonummy ipsum non','2018-07-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-67.40,1,'convallis dolor.','2019-04-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-87.92,1,'a sollicitudin orci sem eget massa. Suspendisse','2018-09-03');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-95.78,1,'luctus. Curabitur egestas nunc sed libero.','2018-04-04');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-83.86,1,'Nunc mauris elit, dictum eu, eleifend nec,','2019-06-04');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-18.01,1,'mollis. Phasellus libero','2018-12-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-65.13,1,'lacus. Mauris non dui nec urna suscipit nonummy.','2018-04-07');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-8.30,1,'consequat nec,','2019-04-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-15.11,1,'lectus. Cum sociis natoque penatibus et magnis dis','2019-04-29');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-27.27,1,'sem elit, pharetra','2018-12-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-1.72,1,'tempor lorem, eget mollis lectus pede et','2018-05-04');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-13.68,1,'lectus sit amet luctus vulputate, nisi sem','2018-10-06');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.71,1,'hendrerit','2018-11-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-70.62,1,'sapien, gravida non, sollicitudin a,','2018-07-09');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-33.47,1,'Nam tempor diam dictum sapien.','2018-09-27');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.17,1,'Praesent eu dui. Cum sociis','2018-11-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-49.59,1,'mauris. Morbi','2018-10-01');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-66.16,1,'dolor quam, elementum at, egestas','2019-05-24');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-95.46,1,'bibendum fermentum metus.','2018-07-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-52.37,1,'fringilla.','2018-05-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-10.98,1,'magnis dis parturient montes, nascetur ridiculus mus. Proin','2018-07-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-30.37,1,'malesuada augue ut lacus. Nulla tincidunt,','2018-04-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-3.73,1,'amet, consectetuer adipiscing elit. Etiam laoreet, libero','2019-03-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-25.83,1,'urna. Nunc','2018-10-06');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.06,1,'Donec luctus aliquet','2018-08-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-96.63,1,'eget nisi dictum augue malesuada','2019-03-22');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-73.54,1,'Fusce','2019-05-28');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-10.46,1,'ante. Nunc mauris sapien, cursus in,','2019-05-19');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-33.48,1,'Cras','2019-02-01');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-80.18,1,'iaculis enim, sit','2018-11-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-89.79,1,'ante ipsum','2018-06-06');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-95.53,1,'ligula eu enim. Etiam imperdiet dictum magna.','2019-01-22');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-54.59,1,'sit amet metus. Aliquam','2019-05-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-88.17,1,'dis parturient montes, nascetur','2018-06-02');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-59.41,1,'bibendum ullamcorper.','2019-04-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-55.25,1,'varius et, euismod','2018-06-16');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-30.31,1,'nulla. Cras eu tellus','2019-03-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-23.15,1,'nisl. Quisque fringilla euismod enim. Etiam gravida','2019-06-03');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-15.35,1,'elit, pellentesque','2018-03-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-92.11,1,'vitae odio sagittis semper. Nam tempor diam','2019-03-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-62.05,1,'nonummy ultricies','2019-04-04');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-21.17,1,'netus et malesuada fames ac','2018-10-17');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-10.19,1,'erat neque non','2018-03-17');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-66.21,1,'ut quam vel sapien','2018-11-29');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-79.33,1,'volutpat ornare, facilisis eget, ipsum. Donec','2018-09-20');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-7.18,1,'ut','2018-05-01');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-93.01,2,'malesuada vel,','2018-06-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-64.56,2,'sociis natoque penatibus et','2018-06-24');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-81.42,2,'sem, vitae aliquam','2018-12-27');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-44.86,2,'eros non enim commodo hendrerit. Donec','2018-04-17');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-55.04,3,'metus urna convallis','2018-07-14');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-88.43,1,'elit. Aliquam auctor, velit eget laoreet posuere, enim','2018-03-22');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-64.35,1,'mattis ornare, lectus ante dictum mi, ac mattis','2019-04-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-65.00,2,'ut cursus','2019-05-19');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-21.04,2,'Maecenas','2018-10-28');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-55.28,2,'rhoncus. Nullam velit','2018-11-07');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-46.26,2,'aliquet odio. Etiam ligula tortor, dictum','2018-10-02');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-44.89,2,'vulputate ullamcorper magna. Sed eu','2018-10-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-46.43,2,'Aliquam','2019-02-04');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-81.57,2,'adipiscing, enim mi tempor lorem, eget mollis lectus','2018-12-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.74,2,'velit.','2018-07-19');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-36.10,2,'lobortis. Class','2018-03-17');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-57.98,2,'aliquet magna a neque. Nullam ut','2018-05-16');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-82.97,2,'Sed eu nibh vulputate mauris sagittis placerat. Cras','2018-04-18');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-92.61,2,'quam. Curabitur','2018-09-03');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-58.42,2,'justo nec ante. Maecenas','2019-03-28');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-32.10,2,'dictum sapien.','2018-07-10');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-77.41,2,'Duis cursus, diam','2018-05-22');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-69.70,2,'Aliquam ultrices iaculis odio. Nam interdum enim','2019-04-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-71.38,2,'tellus. Suspendisse sed dolor.','2018-12-17');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-67.42,2,'ultricies ligula. Nullam enim. Sed nulla ante,','2019-05-24');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-96.79,2,'Integer','2019-02-09');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.96,2,'euismod enim. Etiam gravida molestie arcu. Sed','2018-03-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.36,2,'Cras dictum ultricies ligula. Nullam enim.','2018-11-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-7.09,2,'consectetuer mauris','2018-07-07');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.05,2,'Proin','2018-09-14');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.23,2,'ultrices sit amet, risus. Donec nibh enim,','2019-05-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-7.68,2,'sit amet, risus. Donec nibh','2019-01-04');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.88,2,'montes, nascetur ridiculus mus. Aenean eget magna.','2018-07-20');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.40,2,'eu, placerat eget, venenatis a, magna. Lorem','2018-06-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.59,2,'diam','2018-12-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.31,2,'massa. Suspendisse eleifend. Cras sed leo.','2018-04-20');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.34,2,'parturient montes, nascetur ridiculus mus.','2018-09-08');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-1.75,2,'consectetuer rhoncus. Nullam velit dui,','2019-03-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-7.98,2,'imperdiet nec, leo. Morbi','2018-03-25');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.94,2,'amet luctus vulputate, nisi sem semper erat,','2018-10-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.91,2,'et netus','2018-09-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-7.45,2,'arcu. Sed et libero. Proin mi.','2018-07-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-10.74,2,'amet massa. Quisque porttitor eros','2018-10-22');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.97,2,'Proin mi. Aliquam gravida mauris ut','2019-02-17');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.18,2,'ultricies sem magna nec quam.','2019-02-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-1.38,3,'elit elit fermentum risus, at','2018-12-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.87,3,'auctor non, feugiat nec, diam. Duis mi','2019-01-26');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.98,3,'fringilla.','2019-02-05');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.15,3,'ullamcorper viverra. Maecenas','2019-01-18');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.24,3,'ac metus vitae velit','2019-02-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-13.11,3,'nec, imperdiet nec, leo. Morbi neque','2019-03-21');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.86,3,'mauris sagittis placerat. Cras dictum','2018-11-22');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.32,3,'per inceptos hymenaeos. Mauris ut quam vel sapien','2019-05-08');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.14,3,'dui nec urna suscipit nonummy. Fusce fermentum','2018-11-13');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-8.07,3,'posuere','2019-02-16');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.51,3,'at, iaculis quis, pede. Praesent eu dui.','2018-04-29');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.63,3,'gravida nunc sed pede. Cum sociis','2018-12-26');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.98,3,'iaculis, lacus pede sagittis augue, eu tempor','2019-06-02');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-5.18,3,'non leo. Vivamus nibh dolor,','2018-03-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.54,3,'nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique','2018-06-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.52,3,'justo.','2018-09-24');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-1.95,3,'accumsan interdum libero dui','2019-01-27');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.15,3,'Mauris vestibulum, neque sed dictum eleifend, nunc','2018-12-08');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-7.92,3,'magnis dis parturient montes, nascetur ridiculus','2018-12-17');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.05,3,'sodales','2018-04-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.03,3,'risus. Nulla eget metus','2018-08-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.68,3,'mi','2018-08-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.26,3,'cursus, diam at pretium aliquet, metus urna','2018-12-05');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.81,3,'pellentesque','2019-03-25');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-13.01,3,'vehicula','2018-12-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.49,3,'mi, ac mattis velit justo nec ante.','2018-10-10');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.40,3,'risus. Nunc ac sem ut dolor dapibus gravida.','2018-09-21');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.98,3,'in consequat enim diam','2018-12-19');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.07,3,'mi. Duis risus odio, auctor','2019-02-06');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-5.89,3,'cursus et, eros. Proin','2019-05-24');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-10.76,3,'at sem molestie sodales.','2019-02-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-1.38,3,'at lacus. Quisque purus sapien, gravida non, sollicitudin','2019-04-19');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.90,3,'gravida. Praesent eu nulla at sem molestie','2018-08-30');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.07,3,'fermentum vel, mauris. Integer sem','2018-11-27');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.90,3,'placerat','2019-04-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.35,3,'Vivamus nisi. Mauris nulla.','2018-07-12');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.66,3,'fringilla cursus purus. Nullam scelerisque neque','2018-11-29');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-7.06,3,'nec ante blandit viverra.','2019-01-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.28,3,'ullamcorper, nisl arcu','2019-03-08');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-3.20,3,'Nulla','2019-03-03');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.58,3,'a, arcu. Sed et libero.','2019-01-22');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.09,3,'libero. Donec consectetuer','2018-08-24');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-8.19,3,'lorem, vehicula et, rutrum','2018-05-04');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-1.72,3,'euismod est arcu','2019-03-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.66,3,'tellus. Phasellus','2018-08-09');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.69,3,'eu elit. Nulla facilisi. Sed neque. Sed eget','2018-12-27');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-6.62,3,'Praesent eu dui. Cum','2019-05-10');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.26,3,'aliquam eu, accumsan','2018-07-09');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-5.34,3,'nostra, per inceptos hymenaeos.','2019-01-07');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.79,3,'Proin non massa non','2018-08-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.94,3,'quam. Pellentesque','2019-05-24');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.07,3,'commodo','2018-11-16');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.20,3,'Cras vulputate velit','2018-04-25');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.08,3,'ac urna. Ut tincidunt vehicula risus. Nulla','2018-05-31');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-6.73,3,'dictum eu, placerat eget,','2019-02-08');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.88,3,'ultrices','2018-08-19');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-5.93,3,'Maecenas malesuada','2018-05-07');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.50,3,'sem elit, pharetra ut, pharetra','2018-12-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.43,3,'et, euismod et, commodo at, libero. Morbi','2019-01-14');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.22,3,'dolor. Donec fringilla. Donec feugiat metus','2019-02-10');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.57,3,'velit egestas lacinia. Sed congue, elit','2018-12-05');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.85,3,'cursus non, egestas','2019-04-06');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-14.10,3,'ornare,','2018-11-15');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.70,3,'id','2018-09-16');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.70,3,'auctor odio a','2019-03-27');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-5.60,3,'Sed congue, elit sed consequat auctor, nunc nulla','2018-04-23');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-13.12,3,'massa. Suspendisse eleifend. Cras sed leo. Cras','2018-05-16');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-9.50,3,'Duis risus odio, auctor','2018-09-28');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-4.70,3,'Maecenas mi felis, adipiscing fringilla,','2018-04-14');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-6.30,3,'Aenean sed pede nec ante','2018-05-26');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.02,3,'magna tellus faucibus leo, in lobortis tellus justo','2018-07-26');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.05,3,'nunc interdum feugiat. Sed','2018-08-16');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-6.94,3,'Aenean massa. Integer vitae','2018-05-24');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.27,3,'lorem. Donec elementum,','2018-05-19');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-11.58,3,'leo elementum sem, vitae','2018-10-27');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-0.55,3,'eu dolor egestas','2018-11-20');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.23,3,'penatibus et magnis dis','2018-11-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-6.01,3,'pede. Cum sociis natoque penatibus','2018-12-19');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-12.81,3,'eu nibh vulputate','2018-10-11');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-10.00,3,'congue a,','2019-04-10');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-2.37,3,'nisi magna','2019-05-10');
INSERT INTO fm.tb_finance_transaction (amount,category_id,description,transaction_date) VALUES (-5.44,3,'nisi. Cum sociis','2018-06-16');



INSERT INTO fm.tb_settings(budget) VALUES(450.34);



commit;

-- procedures

CREATE OR REPLACE FUNCTION fm.f_insert_transaction(v_amount real, v_category text, v_description text, v_transaction_date text)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
  i_category integer;
begin
  select id from fm.tb_categories into i_category where category = v_category;
  insert into fm.tb_finance_transaction
	(amount, category_id, description, transaction_date)
	VALUES(v_amount, i_category, v_description, to_date(v_transaction_date, 'YYYY-MM-DD'));
end;
$function$;


CREATE FUNCTION fm.f_get_total_balance() 
  RETURNS double precision
  LANGUAGE plpgsql
  AS $$
  DECLARE
    total_balance DOUBLE PRECISION;
  BEGIN
    SELECT INTO total_balance SUM(amount) FROM fm.tb_finance_transaction;
    raise notice 'Total balance: %', total_balance;
    RETURN total_balance;
  END;
$$;




CREATE OR REPLACE FUNCTION fm.f_get_expense(OUT percentage_array_1 double precision[], OUT category text[])
 RETURNS record
 LANGUAGE plpgsql
AS $function$

declare 
  	sum_all double precision;
  	percentage double precision;

  	cur_result cursor
  		for select sum(amount)/sum_all 
		from fm.tb_finance_transaction 
		where amount < 0
		and category_id is not null
		group by category_id
		order by category_id;
	
  	i integer = 1;
  	percentage_array double precision[];
	
begin
  	select into sum_all sum(amount) 
  		from fm.tb_finance_transaction 
  		where amount < 0 
  		and category_id is not null;
  	
  	open cur_result;
	loop
	  	fetch cur_result into percentage;
	  	exit when not found; 
	  
	  	category[i] := (select tc.category 
	  					from fm.tb_categories as tc
	 					where tc.id = i);
	  	percentage_array[i] := percentage;
	  	raise notice 'category[i], percentage[i]: %, %', category[i], percentage_array[i];
	  	i = i + 1;
	end loop;
  	close cur_result;
 	percentage_array_1 := percentage_array;
end
$function$





