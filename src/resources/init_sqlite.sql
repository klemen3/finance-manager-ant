PRAGMA foreign_keys=ON;

CREATE TABLE finance_transaction (
	id INTEGER PRIMARY KEY, 
	amount INTEGER NOT NULL, 
	description TEXT, 
	transaction_date DATE 
);

CREATE TABLE settings ( 
	id INTEGER PRIMARY KEY, 
	budget REAL NOT NULL 
);

CREATE TABLE categories (
	id INTEGER PRIMARY KEY,
	category TEXT
);



-- income
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("1300","placa","2018-04-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("1340","placa","2018-05-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("1040.5","placa","2018-03-11");


-- expense
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-82.89","Nunc sed orci lobortis augue scelerisque","2018-08-20");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-45.61","Donec est. Nunc ullamcorper, velit in","2019-03-03");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.39","dictum eu, placerat eget, venenatis a, magna. Lorem","2019-01-06");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-83.86","orci. Phasellus dapibus quam","2018-11-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-73.12","amet","2018-11-03");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-98.06","Proin non massa non ante bibendum","2018-09-04");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-70.88","habitant morbi tristique senectus et netus","2018-05-06");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-25.10","vestibulum, neque sed dictum eleifend, nunc risus varius","2019-04-08");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-88.32","eleifend.","2018-12-25");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-67.75","montes, nascetur ridiculus mus. Aenean eget magna.","2018-08-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-58.45","ut aliquam iaculis, lacus pede sagittis augue, eu","2019-02-07");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-96.03","vitae odio","2018-05-22");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-30.06","nec metus facilisis lorem tristique aliquet. Phasellus","2018-05-21");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-37.15","Phasellus dolor elit, pellentesque","2018-10-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-64.05","non, dapibus rutrum, justo. Praesent","2018-08-17");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-27.38","Sed nunc est, mollis","2018-04-08");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-40.65","tellus.","2019-05-06");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-91.17","vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem","2018-04-03");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-45.06","cubilia","2019-02-14");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-21.66","tellus faucibus leo, in lobortis tellus","2018-05-31");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.91","arcu ac orci. Ut semper","2018-09-02");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-95.81","Nunc mauris. Morbi non","2018-04-25");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-80.83","non, bibendum sed, est. Nunc laoreet lectus","2019-02-05");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-33.02","ac","2018-12-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-43.04","vestibulum lorem, sit amet","2019-01-08");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.00","purus, in molestie tortor nibh","2018-05-29");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-96.18","facilisis vitae, orci. Phasellus dapibus quam quis diam.","2018-09-18");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.74","orci. Donec nibh. Quisque nonummy ipsum non","2018-07-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-67.40","convallis dolor.","2019-04-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-87.92","a sollicitudin orci sem eget massa. Suspendisse","2018-09-03");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-95.78","luctus. Curabitur egestas nunc sed libero.","2018-04-04");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-83.86","Nunc mauris elit, dictum eu, eleifend nec,","2019-06-04");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-18.01","mollis. Phasellus libero","2018-12-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-65.13","lacus. Mauris non dui nec urna suscipit nonummy.","2018-04-07");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-8.30","consequat nec,","2019-04-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-15.11","lectus. Cum sociis natoque penatibus et magnis dis","2019-04-29");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-27.27","sem elit, pharetra","2018-12-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-1.72","tempor lorem, eget mollis lectus pede et","2018-05-04");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-13.68","lectus sit amet luctus vulputate, nisi sem","2018-10-06");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.71","hendrerit","2018-11-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-70.62","sapien, gravida non, sollicitudin a,","2018-07-09");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-33.47","Nam tempor diam dictum sapien.","2018-09-27");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.17","Praesent eu dui. Cum sociis","2018-11-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-49.59","mauris. Morbi","2018-10-01");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-66.16","dolor quam, elementum at, egestas","2019-05-24");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-95.46","bibendum fermentum metus.","2018-07-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-52.37","fringilla.","2018-05-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-10.98","magnis dis parturient montes, nascetur ridiculus mus. Proin","2018-07-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-30.37","malesuada augue ut lacus. Nulla tincidunt,","2018-04-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-3.73","amet, consectetuer adipiscing elit. Etiam laoreet, libero","2019-03-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-25.83","urna. Nunc","2018-10-06");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.06","Donec luctus aliquet","2018-08-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-96.63","eget nisi dictum augue malesuada","2019-03-22");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-73.54","Fusce","2019-05-28");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-10.46","ante. Nunc mauris sapien, cursus in,","2019-05-19");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-33.48","Cras","2019-02-01");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-80.18","iaculis enim, sit","2018-11-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-89.79","ante ipsum","2018-06-06");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-95.53","ligula eu enim. Etiam imperdiet dictum magna.","2019-01-22");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-54.59","sit amet metus. Aliquam","2019-05-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-88.17","dis parturient montes, nascetur","2018-06-02");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-59.41","bibendum ullamcorper.","2019-04-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-55.25","varius et, euismod","2018-06-16");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-30.31","nulla. Cras eu tellus","2019-03-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-23.15","nisl. Quisque fringilla euismod enim. Etiam gravida","2019-06-03");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-15.35","elit, pellentesque","2018-03-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-92.11","vitae odio sagittis semper. Nam tempor diam","2019-03-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-62.05","nonummy ultricies","2019-04-04");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-21.17","netus et malesuada fames ac","2018-10-17");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-10.19","erat neque non","2018-03-17");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-66.21","ut quam vel sapien","2018-11-29");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-79.33","volutpat ornare, facilisis eget, ipsum. Donec","2018-09-20");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-7.18","ut","2018-05-01");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-93.01","malesuada vel,","2018-06-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-64.56","sociis natoque penatibus et","2018-06-24");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-81.42","sem, vitae aliquam","2018-12-27");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-44.86","eros non enim commodo hendrerit. Donec","2018-04-17");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-55.04","metus urna convallis","2018-07-14");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-88.43","elit. Aliquam auctor, velit eget laoreet posuere, enim","2018-03-22");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-64.35","mattis ornare, lectus ante dictum mi, ac mattis","2019-04-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-65.00","ut cursus","2019-05-19");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-21.04","Maecenas","2018-10-28");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-55.28","rhoncus. Nullam velit","2018-11-07");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-46.26","aliquet odio. Etiam ligula tortor, dictum","2018-10-02");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-44.89","vulputate ullamcorper magna. Sed eu","2018-10-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-46.43","Aliquam","2019-02-04");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-81.57","adipiscing, enim mi tempor lorem, eget mollis lectus","2018-12-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.74","velit.","2018-07-19");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-36.10","lobortis. Class","2018-03-17");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-57.98","aliquet magna a neque. Nullam ut","2018-05-16");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-82.97","Sed eu nibh vulputate mauris sagittis placerat. Cras","2018-04-18");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-92.61","quam. Curabitur","2018-09-03");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-58.42","justo nec ante. Maecenas","2019-03-28");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-32.10","dictum sapien.","2018-07-10");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-77.41","Duis cursus, diam","2018-05-22");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-69.70","Aliquam ultrices iaculis odio. Nam interdum enim","2019-04-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-71.38","tellus. Suspendisse sed dolor.","2018-12-17");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-67.42","ultricies ligula. Nullam enim. Sed nulla ante,","2019-05-24");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-96.79","Integer","2019-02-09");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.96","euismod enim. Etiam gravida molestie arcu. Sed","2018-03-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.36","Cras dictum ultricies ligula. Nullam enim.","2018-11-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-7.09","consectetuer mauris","2018-07-07");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.05","Proin","2018-09-14");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.23","ultrices sit amet, risus. Donec nibh enim,","2019-05-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-7.68","sit amet, risus. Donec nibh","2019-01-04");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.88","montes, nascetur ridiculus mus. Aenean eget magna.","2018-07-20");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.40","eu, placerat eget, venenatis a, magna. Lorem","2018-06-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.59","diam","2018-12-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.31","massa. Suspendisse eleifend. Cras sed leo.","2018-04-20");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.34","parturient montes, nascetur ridiculus mus.","2018-09-08");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-1.75","consectetuer rhoncus. Nullam velit dui,","2019-03-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-7.98","imperdiet nec, leo. Morbi","2018-03-25");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.94","amet luctus vulputate, nisi sem semper erat,","2018-10-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.91","et netus","2018-09-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-7.45","arcu. Sed et libero. Proin mi.","2018-07-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-10.74","amet massa. Quisque porttitor eros","2018-10-22");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.97","Proin mi. Aliquam gravida mauris ut","2019-02-17");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.18","ultricies sem magna nec quam.","2019-02-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-1.38","elit elit fermentum risus, at","2018-12-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.87","auctor non, feugiat nec, diam. Duis mi","2019-01-26");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.98","fringilla.","2019-02-05");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.15","ullamcorper viverra. Maecenas","2019-01-18");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.24","ac metus vitae velit","2019-02-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-13.11","nec, imperdiet nec, leo. Morbi neque","2019-03-21");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.86","mauris sagittis placerat. Cras dictum","2018-11-22");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.32","per inceptos hymenaeos. Mauris ut quam vel sapien","2019-05-08");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.14","dui nec urna suscipit nonummy. Fusce fermentum","2018-11-13");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-8.07","posuere","2019-02-16");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.51","at, iaculis quis, pede. Praesent eu dui.","2018-04-29");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.63","gravida nunc sed pede. Cum sociis","2018-12-26");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.98","iaculis, lacus pede sagittis augue, eu tempor","2019-06-02");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-5.18","non leo. Vivamus nibh dolor,","2018-03-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.54","nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique","2018-06-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.52","justo.","2018-09-24");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-1.95","accumsan interdum libero dui","2019-01-27");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.15","Mauris vestibulum, neque sed dictum eleifend, nunc","2018-12-08");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-7.92","magnis dis parturient montes, nascetur ridiculus","2018-12-17");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.05","sodales","2018-04-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.03","risus. Nulla eget metus","2018-08-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.68","mi","2018-08-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.26","cursus, diam at pretium aliquet, metus urna","2018-12-05");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.81","pellentesque","2019-03-25");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-13.01","vehicula","2018-12-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.49","mi, ac mattis velit justo nec ante.","2018-10-10");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.40","risus. Nunc ac sem ut dolor dapibus gravida.","2018-09-21");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.98","in consequat enim diam","2018-12-19");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.07","mi. Duis risus odio, auctor","2019-02-06");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-5.89","cursus et, eros. Proin","2019-05-24");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-10.76","at sem molestie sodales.","2019-02-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-1.38","at lacus. Quisque purus sapien, gravida non, sollicitudin","2019-04-19");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.90","gravida. Praesent eu nulla at sem molestie","2018-08-30");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.07","fermentum vel, mauris. Integer sem","2018-11-27");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.90","placerat","2019-04-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.35","Vivamus nisi. Mauris nulla.","2018-07-12");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.66","fringilla cursus purus. Nullam scelerisque neque","2018-11-29");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-7.06","nec ante blandit viverra.","2019-01-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.28","ullamcorper, nisl arcu","2019-03-08");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-3.20","Nulla","2019-03-03");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.58","a, arcu. Sed et libero.","2019-01-22");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.09","libero. Donec consectetuer","2018-08-24");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-8.19","lorem, vehicula et, rutrum","2018-05-04");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-1.72","euismod est arcu","2019-03-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.66","tellus. Phasellus","2018-08-09");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.69","eu elit. Nulla facilisi. Sed neque. Sed eget","2018-12-27");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-6.62","Praesent eu dui. Cum","2019-05-10");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.26","aliquam eu, accumsan","2018-07-09");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-5.34","nostra, per inceptos hymenaeos.","2019-01-07");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.79","Proin non massa non","2018-08-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.94","quam. Pellentesque","2019-05-24");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.07","commodo","2018-11-16");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.20","Cras vulputate velit","2018-04-25");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.08","ac urna. Ut tincidunt vehicula risus. Nulla","2018-05-31");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-6.73","dictum eu, placerat eget,","2019-02-08");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.88","ultrices","2018-08-19");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-5.93","Maecenas malesuada","2018-05-07");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.50","sem elit, pharetra ut, pharetra","2018-12-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.43","et, euismod et, commodo at, libero. Morbi","2019-01-14");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.22","dolor. Donec fringilla. Donec feugiat metus","2019-02-10");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.57","velit egestas lacinia. Sed congue, elit","2018-12-05");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.85","cursus non, egestas","2019-04-06");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-14.10","ornare,","2018-11-15");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.70","id","2018-09-16");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.70","auctor odio a","2019-03-27");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-5.60","Sed congue, elit sed consequat auctor, nunc nulla","2018-04-23");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-13.12","massa. Suspendisse eleifend. Cras sed leo. Cras","2018-05-16");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-9.50","Duis risus odio, auctor","2018-09-28");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-4.70","Maecenas mi felis, adipiscing fringilla,","2018-04-14");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-6.30","Aenean sed pede nec ante","2018-05-26");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.02","magna tellus faucibus leo, in lobortis tellus justo","2018-07-26");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.05","nunc interdum feugiat. Sed","2018-08-16");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-6.94","Aenean massa. Integer vitae","2018-05-24");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.27","lorem. Donec elementum,","2018-05-19");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-11.58","leo elementum sem, vitae","2018-10-27");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-0.55","eu dolor egestas","2018-11-20");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.23","penatibus et magnis dis","2018-11-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-6.01","pede. Cum sociis natoque penatibus","2018-12-19");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-12.81","eu nibh vulputate","2018-10-11");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-10.00","congue a,","2019-04-10");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-2.37","nisi magna","2019-05-10");
INSERT INTO finance_transaction (amount,description,transaction_date) VALUES ("-5.44","nisi. Cum sociis","2018-06-16");



INSERT INTO settings(budget) VALUES(450.34);
